# FER-WEB2-project1

## Projektni zadatak

Potrebno je izraditi web-aplikaciju koja će na početnoj stranici prikazati posjetiteljevu trenutnu lokaciju - brojčano i na karti. Kao podloge za kartu koristiti OpenStreetMap.

Omogućiti prijavu korisnika u aplikaciju korištenjem protokola OpenId Connect (OIDC) i servisa Auth0. Korisnike na servisu Auth0 možete dodati kroz opciju User management/Users na Auth0.

Prijavljenim korisnicima prikazati kartu na kojoj se vide lokacije zadnjih 5 prijavljenih korisnika. Klikom na oznaku treba se prikazati informacija o korisniku na toj lokaciji (ime i/ili e-mail) i vremenu prijave. Popis prijavljenih korisnika ne mora biti trajnog karaktera, tj. može se čuvati u memoriji servera.

const defaultMapPosition = {
    center:[45.800787960600395, 15.971154759530835], 
    zoom:10
}

let map = new L.map('map-private', defaultMapPosition);

let layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
map.addLayer(layer);

const locationText = document.querySelector('#location');

var greenIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

getUsers()
.then(res => {
    res.users.map(user => {
        let marker;
        if (user.email === res.activeUser.email) {
            marker = L.marker([user.latitude, user.longitude]).addTo(map);
            locationText.textContent = `Tvoja trenutna lokacija: ${user.latitude}°, ${user.longitude}°`;
            map.setView(new L.LatLng(user.latitude, user.longitude), 10);
        } else {
            marker = L.marker([user.latitude, user.longitude], {icon: greenIcon}).addTo(map);
        }
        marker.bindPopup(`<b>${user.nickname}</b><br/> ${new Date(user.timestamp).toLocaleString()}`);
    });
});

async function getUsers() {
    const response = await fetch('/users');
    return response.json();
}
const express = require('express');
const path = require('path');
const { auth, requiresAuth } = require('express-openid-connect');
const dotenv = require('dotenv');
var https = require('https');
var fs = require('fs');

dotenv.config();

const config = {
  authRequired: false,
  auth0Logout: true,
  routes: {
    login: false
  },
  secret: process.env.SECRET,
  baseURL: process.env.APP_URL || process.env.BASEURL,
  clientID: process.env.CLIENTID,
  issuerBaseURL: process.env.ISSUERBASEURL,
};

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use("/public", express.static('./public/'));
app.use(express.urlencoded({ extended: true }));
app.use(auth(config));
app.use(express.json());

let users = [];

app.get('/',  function(req, res) { 
  res.render('map', {
    isAuthenticated: req.oidc.isAuthenticated(),
    user: req.oidc.user
  });
});

app.get('/users', requiresAuth(), function(req, res) {
  res.status(200).send({
    users: users,
    activeUser: req.oidc.user
  });
})

app.get('/login', function(req, res) {
  res.oidc.login({
    returnTo: '/user/timestamp'
  });
});

app.get('/user/timestamp', function(req, res) {
  let idx = users.findIndex(u => u.email === req.oidc.user.email); 
  if (idx != -1) {
    users[idx].timestamp = new Date();
  }
  res.redirect('/');
});

app.post('/users/update', function(req, res) {
  if (req.oidc.isAuthenticated()) {
    let user = {
      nickname: req.oidc.user.nickname,
      email: req.oidc.user.email,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      timestamp: new Date(req.body.timestamp)
    }

    if (users.some(u => u.email === user.email)) {
      let idx = users.findIndex(u => u.email === user.email);
      users[idx].latitude = user.latitude;
      users[idx].longitude = user.longitude;
    } else {
      users.splice(0, 0, user);
    }

    users.sort((user1, user2) => user2.timestamp - user1.timestamp);

    if (users.length > 5) {
      users = users.slice(0, 5);
    }
  }

  res.status(200).send();
})

app.get('/private', requiresAuth(), function(req, res) {
  res.render('private', {user: req.oidc.user, users: users});
})

const port = process.env.PORT || 3000;
if (process.env.PORT) {
  app.listen(port, () => console.log('Server running'));
} else {
  https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, () => {
    console.log(`Server running at https://localhost:${port}/`);
  });
}
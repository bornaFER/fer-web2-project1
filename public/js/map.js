function createMap(latitude, longitude) {
    const defaultMapPosition = {
        center:[latitude, longitude], 
        zoom:10
    }
    
    let map = new L.map('map', defaultMapPosition);
    
    let layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    map.addLayer(layer);

    return map;
}


const locationText = document.querySelector('#location');

function success(position) {
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;
    let map = createMap(latitude, longitude);

    locationText.textContent = `Tvoja trenutna lokacija: ${latitude}°, ${longitude}°`;
    new L.Marker([latitude, longitude]).addTo(map);

    updateUsers(position);
}
    
function error() {
    locationText.textContent = 'Nije moguće dohvatiti Vašu lokaciju'; 
}

if(!navigator.geolocation) {
    locationText.textContent = 'Geolokacija nije podržana na Vašem pregledniku';
} else {
    locationText.textContent = 'Lociranje...';
    navigator.geolocation.getCurrentPosition(success, error);
}

async function updateUsers(position) {
    const pos = {
        timestamp: position.timestamp,
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
    }
    
    await fetch('/users/update', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(pos)
    })
    .catch(err => console.log(err));
}
